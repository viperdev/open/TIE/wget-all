import concurrent.futures
import subprocess
from urllib.request import urlopen, Request
from urllib.error import HTTPError, URLError
from ssl import SSLError, CertificateError


def write_url_to_file(file, url):
    with open(file, encoding='utf8', mode='a') as f:
        f.write(url + '\n')


def get_urls(file):
    result = []

    with open(file, encoding='utf8') as f:
        for line in f:
            result.append(line)
    return result


def get_final_url(url):
    try:
        final_url = urlopen(Request(url, headers={'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:57.0) Gecko/20100101 Firefox/57.0'})).geturl()
    except (HTTPError, URLError, SSLError, CertificateError):
        write_url_to_file('failed_convert_url', url)
        return ''

    return final_url


def scrape_website(url):
    command = ['wget', '--recursive', '--level=1', '--no-clobber', '--html-extension', '--timeout=5', '--no-parent',
               '--waitretry=1', '--tries=2', '--user-agent=Mozilla',
               '--exclude-directories=wp-json,wp-content,wp-includes',
               '--reject=aac,abw,arc,avi,ashx,azw,bin,bz,bz2,csh,css,csv,doc,docx,eot,epub,gif,ico,ics,jar,jpeg,jpg,js,'
               'json,mid,midi,mp3,mp4,mpeg,mpkg,odp,ods,odt,oga,ogv,ogx,otf,png,pdf,ppt,rar,rtf,sh,svg,swf,tar,tif,'
               'tiff,ts,ttf,vsd,wav,weba,webm,webp,woff,woff2,xls,xlsx,xml,xul,zip,3gp,3g2,7z',
               '--execute', 'robots=off',
               '--directory-prefix=output', url]
    subprocess.run(command)


urls = get_urls('nonprofits.txt')

with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    future_to_url = {executor.submit(scrape_website, get_final_url(url)): url for url in urls}
